<?php

namespace App\Controller;

use App\Command\AddNote;
use App\Command\GetProducts;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\SerializerInterface;

class LuckyController
{
    use HandleTrait;

    public function __invoke(MessageBusInterface $bus, MessageBusInterface $queryBus, SerializerInterface $serializer): Response
    {
        $bus->dispatch(new AddNote('1', '2', '3'));

        $this->messageBus = $queryBus;
        $products = $this->handle(new GetProducts());

        return JsonResponse::fromJsonString(
            $serializer->serialize($products, 'json')
        );
    }
}
