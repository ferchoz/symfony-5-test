<?php
namespace App\Command;


class AddNote
{
    private $id;
    private $title;
    private $description;
    public function __construct(string $noteId, string $noteTitle, string $noteDescription)
    {
        $this->id = $noteId;
        $this->title = $noteTitle;
        $this->description = $noteDescription;
    }
    public function getId(): string
    {
        return $this->id;
    }
    public function getTitle(): string
    {
        return $this->title;
    }
    public function getDescription(): string
    {
        return $this->description;
    }
}
