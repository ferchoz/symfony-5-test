<?php
namespace App\Command\Handler;

use App\Command\AddNote;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AddNoteHandler implements MessageHandlerInterface
{
    private $entityManager;
    
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(AddNote $command)
    {
        $noteId = $command->getId();
        $noteTitle = $command->getTitle();
        $noteDescription = $command->getDescription();

        $product = new Product();
        $product->setName('Keyboard');
        $product->setPrice(1999);

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $this->entityManager->persist($product);
    }
}