<?php
namespace App\Command\Handler;

use App\Command\GetProducts;
use App\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GetProductsHandler implements MessageHandlerInterface
{
    private $productRepository;
    
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function __invoke(GetProducts $command)
    {
        return $this->productRepository->findAll();
    }
}